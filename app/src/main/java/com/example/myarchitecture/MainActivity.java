package com.example.myarchitecture;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.myarchitecture.ui.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationController.navigateToMainFragment();

//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        MainFragment mainFragment = new MainFragment();
////                .addToBackStack(null)
//                fragmentTransaction.replace(R.id.container, mainFragment)
//                .commit();
    }
}
