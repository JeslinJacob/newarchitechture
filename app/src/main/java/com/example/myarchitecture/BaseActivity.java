package com.example.myarchitecture;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.myarchitecture.common.NavigationController;
import com.example.myarchitecture.interfaze.OnFragmentInteractionListener;

public class BaseActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    protected NavigationController navigationController;
    private ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationController= new NavigationController(this);
    }


    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressDialog(boolean canceleable, String message) {
        if (mProgressDialog == null) mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(canceleable);
        mProgressDialog.show();
    }

    @Override
    public void cancelProgressDialog() {
        if (mProgressDialog != null) mProgressDialog.cancel();
    }

    @Override
    public void setTitle(String title) {
//        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
//        mTitle.setText(title);
    }
}
