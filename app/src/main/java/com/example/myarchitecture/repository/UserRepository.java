package com.example.myarchitecture.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.myarchitecture.ApplicationClass;
import com.example.myarchitecture.common.Resource;
import com.example.myarchitecture.model.LoginModel;
import com.example.myarchitecture.network.ApiInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {

    ApiInterface apiInterface = ApplicationClass.getApp().getApiInterface();

    public LiveData<Resource<ResponseBody>> login(LoginModel loginModel){

        MutableLiveData<Resource<ResponseBody>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));
        apiInterface.login(loginModel)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful())
                            data.setValue(Resource.success(response.body()));
                        else
                            data.setValue(Resource.error("Login failed", null));
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        data.setValue(Resource.error("Login error", null));
                    }
                });
        return data;
    }
}
