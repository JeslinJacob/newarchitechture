package com.example.myarchitecture.network;
import android.net.Uri;

import java.io.IOException;
import java.util.Set;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

// when we send a response we need to collect all the cookies we have with us ,
//add it to the header of the request and send it to server
public class CookiesAddInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        // geting the compleate url
        String url = chain.request().url().toString();
        // extracting only the host part for verifications
        String host = Uri.parse(url).getHost();
        Request.Builder builder = chain.request().newBuilder();
        // all our cookies are stored inside a shared preferance
        // we use CokkieStore.getCookies to get the Set of all cookies we have stored
        Set<String> preferences = CookieStore.getCookies(host);

        // using enhanced for loop we add each cookie to the header of the request we are sending to the server
        for (String cookie : preferences) {
            builder.addHeader("Cookie", cookie);
        }
        return chain.proceed(builder.build());
    }
}