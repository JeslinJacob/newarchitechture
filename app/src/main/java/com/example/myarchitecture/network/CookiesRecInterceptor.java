package com.example.myarchitecture.network;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;


import com.example.myarchitecture.ApplicationClass;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

// this is an intercepter we need when we are receiving some response
public class CookiesRecInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        String url = chain.request().url().toString();
        String host = Uri.parse(url).getHost();
        Response originalResponse = chain.proceed(chain.request());
        String csrf = "";

        // this is the sharedPreference were we store all the cookies
        SharedPreferences mDefaultPreferance = ApplicationClass.getApp().getSharedPreferences("TokenPref", Context.MODE_PRIVATE);

        // if the cookies in the header is not empty , we have to iterate through the whole
        // header and search for header element " csrftoken "
        // when csrftoken is found we store it into the sharedPreferance so that we can use the token
        //throughout the application
        if (!originalResponse.headers("Set-Cookie").isEmpty()) {

            for(String headerElement : originalResponse.headers("Set-Cookie")){

                if(headerElement.contains("csrftoken")){
                    CookieStore.setCookies(host, new HashSet<>(originalResponse.headers("Set-Cookie")));
                    String[] split = headerElement.split("=");
                    csrf = split[1].split(";")[0];
                    mDefaultPreferance.edit().putString("CSRF",csrf).apply();
                }
            }

        }
        return originalResponse;
    }
}