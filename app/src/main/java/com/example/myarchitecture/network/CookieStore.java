package com.example.myarchitecture.network;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


import com.example.myarchitecture.ApplicationClass;

import java.util.HashSet;
import java.util.Set;

// this is the class we use for managing the cookies stored in the sharedPreferenace

public class CookieStore {

    private static SharedPreferences sDefaultPreferences;

    private static SharedPreferences getDefaultPreferences() {
        if (sDefaultPreferences == null) {
            sDefaultPreferences = ApplicationClass.getApp().getSharedPreferences("cookies_manager", Context.MODE_PRIVATE);
        }
        return sDefaultPreferences;
    }

    private static final Object LOCK = new Object();

    public static void setCookies(String host, Set<String> cookies) {
        synchronized (LOCK) {

            // here we have a set of cookies , we store the set in the preferance with hostname as the key
            getDefaultPreferences().edit().putStringSet(host, cookies).apply();
        }
    }


    public static String getCookiesString(String host) {
        synchronized (LOCK) {
            Set<String> set = getDefaultPreferences().getStringSet(host, null);
            return set != null ? TextUtils.join("; ",set) : "";
        }
    }

    public static Set<String> getCookies(String host) {
        synchronized (LOCK) {
            return getDefaultPreferences().getStringSet(host, new HashSet<String>());
        }
    }

    // during logout we have to call this method to clear all the cookies and token
    public static void clearCookies(){
        getDefaultPreferences().edit().clear().apply();
    }
}