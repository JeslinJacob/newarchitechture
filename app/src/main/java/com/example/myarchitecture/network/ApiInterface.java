package com.example.myarchitecture.network;


import com.example.myarchitecture.model.LoginModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("login/")
    Call<ResponseBody> login(@Body LoginModel loginModel);

//
//    @POST("auth/oauth/token")
//    Call<User> login(@Query("grant_type") String grantType,
//                     @Query("username") String username,
//                     @Query("password") String password);
//
//    @POST("auth/oauth/token")
//    Call<User> getRefreshedTokens(@Query("grant_type") String grantType,
//                                  @Query("refresh_token") String refreshToken);
//
//    @GET("services/v1/user/")
//    Call<UserData> getUserProfile();
//
//    @POST("services/v1/user")
//    Call<ResponseBody> signUp(@Body SignUpRequest signUpRequest);
//
//    @GET("/forgot-password")
//    Call<ResponseBody> forgotPassword(@Query("email") String email);
//
//    @PUT("services/v1/user/")
//    Call<ResponseBody> updateProfile(@Body ProfileUpdateRequest updateRequest);
//
//    @POST("/otp-verify")
//    Call<ResponseBody> verifyOtp(@Body OTPVerifyRequest otpVerifyRequest);
//
//    @GET("services/v1/user/devices")
//    Call<List<DeviceItem>> getDevices();
//
//    @GET("services/v1/user/groups")
//    Call<List<Group>> getGroups();
//
//    @GET("services/v1/user/elements")
//    Call<DeviceItem> getElements();
//
//    @POST("services/v1/user/groups")
//    Call<AddGroupResponse> addGroup(@Body AddGroup group);
//
//    @POST("services/v1/user/devices/validate_device")
//    Call<DeviceData> verifySerial(@Body AddDeviceViewModel.DeviceInfo group);
//
//    @POST("services/v1/user/devices")
//    Call<Device> addDeviceToUser(@Body DeviceInfo group);
//
//    @GET("{ip}/__SL_G_N.D")
//    Call<String> getLocalMACId2(@Query("ip") String ip);
//
//
//    @GET
//    Call<String> getLocalMACId(@Url String ip);
//
//    @GET("__SL_G_N.D")
//    Call<String> getLocalMACId3();
//
//    @POST("api/1/wlan/en_ap_scan")
//    Call<String> scanLocalWifiList(@Body String body);
//
//    @POST("api/1/wlan/profile_del_all")
//    Call<String> deleteExistingProfile();
//
//    @POST("api/1/wlan/confirm_req")
//    Call<String> conformWifiSave();
//
//    @POST("api/1/wlan/profile_add")
//    Call<String> saveWifiInfoToDevice(@Body String body);
//
//    @GET("netlist.txt")
//    Call<String> getLocalWifiList();
//
//    @POST("services/v1/user/groups")
//    Call<ResponseBody> addGroupElements(@Body Group groupItem);
//
//    //    @PUT("services/v1/user/groups/{groupId}")
////    Call<ResponseBody>  addGroupElements(@Path ("groupId") String groupId,@Body GroupItem groupItem);
//    @POST("services/v1/user/groups/1/")
//    Call<ResponseBody> addGroupElements(@Body AddGroupDevices addGroupDevices);
//
//    @POST("services/v1/user/groups")
//    Call<String> addUserGroup(@Body Group group);
//
//    @PUT("services/v1/user/groups/{groupId}")
//    Call<String> editUserGroup(@Path("groupId") String groupId, @Body Group scene);
//
//    @DELETE("services/v1/user/groups/{groupId}")
//    Call<String> deleteUserGroup(@Path("groupId") Integer groupId);
//
//    @POST("services/v1/user/scenes")
//    Call<ResponseBody> addScenes(@Body Scene scene);
//
//    @PUT("services/v1/user/devices/{deviceId}/nodes/{nodeId}")
//    Call<String> updateNode(@Path("deviceId") Integer deviceId, @Path("nodeId") Integer nodeId, @Body Node node);
//
//    @PUT("services/v1/user/scenes/{sceneId}")
//    Call<ResponseBody> editScene(@Path("sceneId") String sceneId, @Body Scene scene);
//
//    @DELETE("services/v1/user/scenes/{sceneId}")
//    Call<ResponseBody> deleteScene(@Path("sceneId") String sceneId);
//
//    @POST("services/v1/user/change_password")
//    Call<ResponseBody> changePassword(@Body ChangePassword changePassword);
//
//    @POST("services/v1/user/activate_user")
//    Call<ResponseBody> resendEmail(@Body EmailModel resend_email_str);
//
//
//    @PATCH("services/v1/user/devices/{deviceId}/nodes/{nodeId}")
//    Call<ResponseBody> renameSwitch(@Path("deviceId") Integer deviceId, @Path("nodeId") Integer nodeId, @Body SwitchRename switch_rename);
//
//
//    @PATCH("https://www.texaconnect.com/services/v1/user/devices/{deviceId}")
//    Call<ResponseBody> renameDevice(@Path("deviceId") Integer deviceId, @Body DeviceRename deviceRename);
//
//    @DELETE("services/v1/user/shared_devices/{sharedDeviceId}")
//    Call<ResponseBody> deleteSharedDevice(@Path("sharedDeviceId") Integer shared_device_id);
//
//    @GET("services/v1/user/shared_devices")
//    Call<List<SharedDevice>> sharedUsers();
//
//
//    @POST("services/v1/user/shared_devices")
//    Call<ResponseBody> shareDevice(@Body ShareDevice shareDevice);
}
