package com.example.myarchitecture.ui;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myarchitecture.ApplicationClass;
import com.example.myarchitecture.BaseFragment;
import com.example.myarchitecture.R;
import com.example.myarchitecture.common.Status;
import com.example.myarchitecture.databinding.FragmentLoginBinding;
import com.example.myarchitecture.databinding.FragmentMainBinding;
import com.example.myarchitecture.model.LoginModel;
import com.example.myarchitecture.viewmodels.UserViewModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment {

    UserViewModel userViewModel = new UserViewModel();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getLoginData().observe(this,response->{
            if (response.status == Status.SUCCESS) {
                mListener.showToast("success");

            } else if (response.status == Status.LOADING) {
                mListener.showToast("loading");
            } else {
                mListener.showToast("error logging in");

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentLoginBinding databinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_login,
                container, false, dataBindingComponent);

        databinding.signin.setOnClickListener(v->{

            String username = databinding.usernametxt.getText().toString();
            String pass=databinding.passtxt.getText().toString();
            userViewModel.setLogin(username,pass);
        });

        return  databinding.getRoot();
    }

}
