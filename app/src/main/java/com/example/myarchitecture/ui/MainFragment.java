package com.example.myarchitecture.ui;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.myarchitecture.ApplicationClass;
import com.example.myarchitecture.BaseFragment;
import com.example.myarchitecture.R;
import com.example.myarchitecture.databinding.FragmentMainBinding;
import com.example.myarchitecture.interfaze.MainPageCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends BaseFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentMainBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main,
                container, false, dataBindingComponent);


        dataBinding.setMainCallBack(callback);
        View view= inflater.inflate(R.layout.fragment_main, container, false);
        Button button = view.findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.showToast("hai");
            }
        });
        return  dataBinding.getRoot();
    }

    MainPageCallback callback = new MainPageCallback() {
        @Override
        public void onLoginClick() {
            navigationController.navigateToLoginFragment();
//            Toast.makeText(getContext(), "asdha", Toast.LENGTH_SHORT).show();
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            LoginFragment loginFragment = new LoginFragment();
//            fragmentManager.beginTransaction()
////                .addToBackStack(null)
//                    .replace(R.id.container, loginFragment)
//                    .commit();

        }

        @Override
        public void onRegClick() {
            navigationController.navigateToRegistrationFragment();
        }

        @Override
        public void onJustClick() {
            mListener.showToast("Hai");
        }
    };

}
