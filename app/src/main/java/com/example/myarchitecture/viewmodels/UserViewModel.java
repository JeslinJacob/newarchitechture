package com.example.myarchitecture.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.example.myarchitecture.common.AbsentLiveData;
import com.example.myarchitecture.common.Resource;
import com.example.myarchitecture.model.LoginModel;
import com.example.myarchitecture.repository.UserRepository;

import okhttp3.ResponseBody;

public class UserViewModel extends ViewModel {

    UserRepository userRepository = new UserRepository();
    //act as trigger
    private final MutableLiveData<LoginModel> login = new MutableLiveData<>();
    //gets the response
    private LiveData<Resource<ResponseBody>> loginResponse;

    public UserViewModel(){
        loginResponse = Transformations.switchMap(login, data -> {
            if (data == null) {
                return AbsentLiveData.create();
            } else {
                return userRepository.login(new LoginModel(data.getUserName(),data.getPassword()));
            }
        });
    }

    public void setLogin(String username,String password){
        login.setValue(new LoginModel(username,password));
    }

    public LiveData<Resource<ResponseBody>> getLoginData() {

        return loginResponse;
    }
}
