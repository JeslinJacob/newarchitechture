package com.example.myarchitecture.common;

public enum Status {
    SUCCESS,
    ERROR,
    ERROR_NETWORK,
    LOADING
}
