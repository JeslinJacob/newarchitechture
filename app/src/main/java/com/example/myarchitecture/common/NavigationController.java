package com.example.myarchitecture.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.example.myarchitecture.MainActivity;
import com.example.myarchitecture.R;
import com.example.myarchitecture.ui.LoginFragment;
import com.example.myarchitecture.ui.MainFragment;
import com.example.myarchitecture.ui.RegistrationFragment;


public class NavigationController {

    protected final int containerId;
    protected final FragmentManager fragmentManager;


    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public NavigationController(FragmentActivity activity) {
        this.containerId = R.id.container;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    public void popOneStepBack() {
        fragmentManager.popBackStack();
    }

    public void popBackAll() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void navigateToMainFragment() {

        popBackAll();
        MainFragment mainFragment = new MainFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, mainFragment)
                .commit();
    }

    public void navigateToLoginFragment() {
        LoginFragment loginFragment = new LoginFragment();
        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(containerId, loginFragment)
                .commit();
    }

    public void navigateToRegistrationFragment() {
        RegistrationFragment registrationFragment = new RegistrationFragment();
        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(containerId, registrationFragment)
                .commit();
    }



}