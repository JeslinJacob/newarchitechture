package com.example.myarchitecture.interfaze;

public interface MainPageCallback {

    void onLoginClick();
    void onRegClick();
    void onJustClick();
}
