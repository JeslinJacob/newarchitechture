package com.example.myarchitecture.interfaze;

public interface OnFragmentInteractionListener {
//    void onRequestPermission(PermissionCallback permissionCallback, String... permission);

    void showToast(String message);

    void showProgressDialog(boolean canceleable, String message);

    void cancelProgressDialog();

    void setTitle(String title);

}
