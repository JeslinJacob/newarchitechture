package com.example.myarchitecture;

import android.app.Application;

import com.example.myarchitecture.network.ApiInterface;
import com.example.myarchitecture.network.RestClient;


public class ApplicationClass extends Application {
    private static ApplicationClass sInstance;
    private ApiInterface apiInterface ;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        sInstance = this;
        apiInterface =  RestClient.getClient().create(ApiInterface.class);
    }

    public static ApplicationClass getApp() {
        return sInstance;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }

//    public boolean isNetworkConnected(Context context) {
//        try {
//            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//            boolean res=activeNetwork != null &&
//                    activeNetwork.isConnectedOrConnecting();
//            Log.d("string","result is : "+res);
//            return activeNetwork != null &&
//                    activeNetwork.isConnectedOrConnecting();
//
//        } catch (Exception e) {
//            return true;
//        }
//
//
//
//    }

}
