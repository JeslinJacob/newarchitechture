package com.example.myarchitecture;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.example.myarchitecture.binding.FragmentDataBindingComponent;
import com.example.myarchitecture.common.NavigationController;
import com.example.myarchitecture.interfaze.OnFragmentInteractionListener;

public class BaseFragment extends Fragment {

    protected OnFragmentInteractionListener mListener;
    //    @Inject
    protected NavigationController navigationController;
    protected android.databinding.DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        navigationController = new NavigationController(getActivity());
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
